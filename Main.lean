-- import libfoo
import Init.System.IOError

@[extern "print_a_uint64"]
opaque print_a_uint64 : @& UInt64 -> BaseIO Unit

@[extern "print_a_int"]
opaque print_a_int : @& Int -> IO Unit

@[extern "print_a_nat"]
opaque print_a_nat : @& Nat -> IO Unit

-- def handle_error (error : IO.Error) : BaseIO Unit
-- | _ => ()

def main : IO Unit := do
  print_a_int 763124573620
  print_a_int 763124573620672130453762537615137657
  print_a_uint64 763124573620
  print_a_nat 763124573620
  
  print_a_nat 7631245736207613204573625413766723456725763

  IO.println "Finish!"
