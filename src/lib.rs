#![feature(let_else)]

use lean_sys::*;

#[no_mangle]
unsafe extern "C" fn print_a_nat(i: b_lean_obj_arg, _world: b_lean_obj_arg) -> lean_obj_res {
    let true = lean_is_scalar(i) else {
        // println!("nat-error");
        let reason = mk_string("Nat too big");
        // println!("Error!");
        let error = lean_mk_io_user_error(reason);
        return lean_io_result_mk_error(error);
    };

    let i = lean_scalar_to_int64(i);
    println!("{}", i);

    lean_io_result_mk_ok(lean_box(0))
}

unsafe fn mk_string(s: &str) -> *mut lean_object {
    lean_mk_string_from_bytes(s.as_ptr(), s.len())
}

#[no_mangle]
unsafe extern "C" fn print_a_int(i: b_lean_obj_arg, _world: b_lean_obj_arg) -> lean_obj_res {
    // println!("{}", i);
    println!("Unsupported. Lean Int is inductive. No convienient API exist. Int64 doesn't have simple C ABI either.");
    lean_io_result_mk_ok(lean_box(0))
}

#[no_mangle]
unsafe extern "C" fn print_a_uint64(i: u64, _world: b_lean_obj_arg) -> lean_obj_res {
    println!("{}", i);
    lean_io_result_mk_ok(lean_box(0))
}
