# trivial lean-sys example

This repo is both a Rust crate and Lean Lake package. This attempt to use `lean-sys` to bind stuff.

This program prints Integers from Lean in Rust.

To run this:
```
cargo build # build target/debug/libfoo.a
lean exe lean-sys-test
```

If you are not on Linux, the rust static library may have a different name. Change that in `lakefile.lean`.


To check generated C function signature (from Lean source):
```
lake env lean -cout.c Main.lean
```
